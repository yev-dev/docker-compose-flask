FROM python:3.7.2-slim


RUN mkdir /opt/counter_app

# We are going to start all the command from here
WORKDIR /opt/counter_app

ADD requirements.txt .

RUN pip install -r requirements.txt


# Add all filed to the directory
ADD . .

EXPOSE 5000


# execute at the end - execute the commands. We will span the application with manage.py run servers using the Flask-script
CMD ["python", "manage.py", "runserver"]

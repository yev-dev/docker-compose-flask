

* Build compose file:

```bash

docker-compose build
```

* To start it up

```bash
docker-compose up
```

* To initialize the database

```bash
docker exec -it counterapp_web_1 python dbunit.py
```

* Interaction with the container

```bash
docker exec -it counterapp_web_1 bash
```